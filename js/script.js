/* BEM VINDO, MARINHEIRO 
   O desafio demorou mas saiu rápido
   Antes de tudo, leia todas as dicas e obersações no link do desafio
   ...
   *-* Todo conteudo dentro de $(document).ready( function() { ... } ); será execultado assim que carregar a página
*/
$(document).ready(function() {
    //Inserir um comando para deixar a div #alerta movel  (Dica: função da jqueryui)
    //chamar a funcão chamada "contador"
    //Fazer a alerta aparecer depois de 5 segundos, chamando ã função toggleAlert

    //Personalização com tooltip
    $(document).tooltip();
      
    $("#alerta").draggable();

    $("i").on("click",function(){
        $("#alerta").slideToggle();   
    });

    contador();
    $("#alerta").delay(6000).slideToggle();  

    $("#novidadesform [type='submit']").click(function(e) {
        e.preventDefault();
        
        let email = $("#novidadesform [name='email']").val();
        
        if(email == ""){
            toastr.error('Preencha um email!', 'Error!')
        }else{
            $.ajax({
                 url: 'http://51.254.204.44/ti/enviar_email.php',
                 type: 'post',
                 data: {'meuemail': email},
                 dataType: 'JSON',  
                 success: function(resposta){
                    toastr.success(resposta.txt)
                    $(".resultado").html("*emaildigitado* foi salvo em nossa lista de novidades =)") 
                    $("#alerta").delay(2000).slideToggle()
                 },
                 error:function(error){
                    console.log(error)
                    toastr.error(error.responseJSON.text,'Error404!')
                 },
            });
        }
    });
});

/* NÃO MEXER 
   Se tiver visível, após executar a função, a div será oculta e vice-versa
*/
function toggleAlert() {
    $('#alerta').slideToggle();
}

//Contador inicia em 5
var i = 5;

function contador() {
    var laço = setInterval(function(){
        $("#contador").html("O Alerta aparecerá em :"+i);
        if(i<=3){
            $("#contador").css("color","#FF1493");
        }
        if(i == 0){
           $("#contador").slideToggle();
           clearInterval();    
        }
        i--
    },1000);
 } 

